/**
 * @file
 * @brief Person struct and utils header file
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P5
 */
#ifndef PERSON_H_
#define PERSON_H_

	/** @brief Person struct */
	typedef struct {
		/** @brief pointer to the string with lastName */
		char* lastNamePtr;
		/** @brief pointer to the string with firstName */
		char* firstNamePtr;
		/** @brief pointer to the int with age */
		int *agePtr;
	} Person;

	/**
	 * @brief Constructs a Person object
	 *
	 * @param[in] lname
	 * @param[in] fname
	 * @param[in] age
	 * @return Person
	 */
	Person *createPerson(char lname[], char fname[], int age);

	/**
	 * @brief Destructs a Person object
	 *
	 * Frees the allocated memory from malloc of a person struct
	 *
	 * @param[in] personPtr
	 */
	void destructPerson(Person *personPtr);

	/**
	 * @brief Compares two persons
	 *
	 * -1 if p1Ptr is less than p2Ptr
	 * 0 if p1Ptr is equals to p2Ptr
	 * 1 if p2Ptr is less tha p1Ptr
	 *
	 * @param[in] p1Ptr
	 * @param[in] p2Ptr
	 *
	 * @return int
	 */
	int personcmp(Person* p1Ptr, Person* p2Ptr);

	/**
	 * @brief Formates a person object to a string
	 *
	 *
	 * @param[in] personPtr
	 *
	 * @return char
	 */
	char *personf(Person* personPtr);

#endif /* PERSON_H_ */
