#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "list.h"
#include "person.h"

/**
 * @file
 * @brief CLI for person administration
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P5
 */

/** @brief the allocated list */
List *listPtr = NULL;
/** @brief identifiert to detect if the program should be ended */
bool programEnd = false;

/**
 * @brief shows the current list entries to the console
 */
void showList() {
	printf("Showing all registered persons.\n");
	printList(listPtr);
	printf("\n");
}

/**
 * @brief allows to insert a person into the list
 */
void insertPersonToList() {
    char *lname = (char*) malloc(50);
	char *fname = (char*) malloc(50);
	int age = 0;
	printf("Insert a new Person\n");
	printf("Last name (max 50 chars): ");
	fflush(stdout);
	scanf("%51s", lname);
	printf("First name (max 50 chars): ");
	fflush(stdout);
	scanf("%51s", fname);
	printf("Age: ");
	fflush(stdout);
	scanf("%3d", &age);

	Person *person = createPerson(lname, fname, age);
	printf("\nNew Person inserted!\nLast-Name: %s\nFirst-Name: %s\nAge: %d\n", lname, fname, age);
	addToList(listPtr, person);
	showList();
}

/**
 * @brief allows to remove a person from the list
 */
void removePersonFromList() {
	if(listPtr->size == 0) {
		printf("\nNothing to remove list!\nList is empty!\n\n");
		return;
	}

	printf("\nSelect the entry index you want to delete\n");
	printList(listPtr);
	printf("Index to delete: ");
	fflush(stdout);
	int index;
	scanf("%10000d",&index);

	if(index < 1 || index > listPtr->size) {
		printf("Invalid input!");
		removePersonFromList();
		return;
	}

	printf("\nDeleting index %d\n", index);
	removeFromListByIndex(listPtr, index - 1);
	showList();
}

/**
 * @brief allows to clear the list
 */
void resetList() {
	printf("\nDeleting all the entries...\n");
	clearList(listPtr);
	showList();
}

/**
 * @brief utility to read a command from the console
 *
 * @return char
 */
char readCommand(){
	char *command = (char*) malloc(1);
	printf("Enter a command.\n\t(i)nsert\n\t(r)emove\n\t(s)how\n\t(c)lear\n\t(e)nd\nCommand: ");
	fflush(stdout);
	scanf("%1s", command);

	return command[0];
}

/**
 * @brief Main entry point to manipulate a lists
 *
 * @return EXIT_SUCCESS
 */
int main(void) {
	listPtr = createList();

	bool programEnd = false;

	while (!programEnd) {
		char command = readCommand();
		switch (command) {
			case 'I':
			case 'i':
				insertPersonToList();
				break;
			case 'R':
			case 'r':
				removePersonFromList();
				break;
			case 'S':
			case 's':
				showList();
				break;
			case 'C':
			case 'c':
				resetList();
				break;
			case 'E':
			case 'e':
				programEnd = true;
				printf("Bye Bye!\n");
				break;
			default:
				printf("Invalid input!\n");
				break;
		}
	}

	return EXIT_SUCCESS;
}

