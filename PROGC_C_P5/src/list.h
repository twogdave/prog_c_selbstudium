/**
 * @file
 * @brief List and ListElement struct and utils header file
 *
 * Doubly linked list
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P5
 */

#ifndef LIST_H_
#define LIST_H_

#include <stdbool.h>
#include "person.h"

	/** @brief struct that represents a list element */
	typedef struct LE {
		/** @brief pointer to the allocated Person */
		Person *personPtr;
		/** @brief pointer to next list element */
		struct LE *nextPtr;
		/** @brief pointer to previous list element */
		struct LE *prevPtr;
	} ListElement;

	/** @brief struct that represents a list  */
	typedef struct L {
		/** @brief pointer to the head of the list */
		ListElement *headPtr;
		/** @brief size of the list */
		int size;
	} List;

	/**
	 * @brief Constructs a List object with headerElement
	 *
	 * @return List
	 */
	List *createList();

	/**
	 * @brief Adds a person to the list
	 *
	 * @param[in] listPtr
	 * @param[in] personPtr
	 */
	void addToList(List *listPtr, Person *personPtr);

	/**
	 * @brief removes a person from the list
	 *
	 * @param[in] listPtr
	 * @param[in] personPtr
	 */
	void removeFromList(List *listPtr, Person *personPtr);

	/**
	 * @brief removes a person from the list by indexing
	 *
	 * @param[in] listPtr
	 * @param[in] index
	 */
	void removeFromListByIndex(List *listPtr, int index);

	/**
	 * @brief clears the list, all element are freed
	 *
	 * @param[in] listPtr
	 */
	void clearList(List *listPtr);

	/**
	 * @brief return a person object a given index the list, all element are freed
	 *
	 * @param[in] listPtr
	 * @param[in] index
	 * @return Person
	 */
	Person *getPersonAtIndex(List *listPtr, int index);

	/**
	 * @brief prints the list elements
	 *
	 * @param[in] listPtr
	 */
	void printList(List *listPtr);

#endif /* LIST_H_ */


