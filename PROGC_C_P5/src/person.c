#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "person.h"

/**
 * @file
 * @brief Implementation of Person utilities
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P5
 */

Person* createPerson(char lname[], char fname[], int age) {
	char* lnamePtr = (char*) malloc(sizeof(*lname));
	strcpy(lnamePtr, lname);
	char* fnamePtr = (char*) malloc(sizeof(*fname));
	strcpy(fnamePtr, fname);
	int* agePtr = (int*) malloc(sizeof(age));
	*agePtr = age;

	Person person = { .lastNamePtr=lnamePtr, .firstNamePtr=fnamePtr, .agePtr=agePtr };
	Person* personPtr = (Person*) malloc(sizeof(person));
	*personPtr = person;
	return personPtr;
}

int personcmp(Person *person1Ptr, Person *person2Ptr) {

	if(person1Ptr == NULL && person2Ptr == NULL) {
		return 0;
	} else if (person1Ptr == NULL && person2Ptr != NULL) {
		return -1;
	} else if (person1Ptr != NULL && person2Ptr == NULL) {
		return 1;
	}

	int result = strcasecmp(person1Ptr->lastNamePtr, person2Ptr->lastNamePtr);
	if(result == 0) {
		result = strcasecmp(person1Ptr->firstNamePtr, person2Ptr->firstNamePtr);
		if(result == 0) {
			int ageP1 = *(person1Ptr->agePtr);
			int ageP2 = *(person2Ptr->agePtr);
			result = ageP1 < ageP2 ? -1 : ageP2 < ageP1 ? 1 : 0;
		}
	}
	return result;
}

char* personf(Person *personPtr) {
	char ageStr[sizeof(*(personPtr->agePtr))];
	sprintf(ageStr, "%3d", *(personPtr->agePtr));

	int totalLen = strlen(personPtr->lastNamePtr) + strlen(personPtr->firstNamePtr) + strlen(ageStr) + 2;
	char *concat;
	concat = (char*) malloc(totalLen);
	strcpy(concat, personPtr->lastNamePtr);
	strcat(concat, " ");
	strcat(concat, personPtr->firstNamePtr);
	strcat(concat, " ");
	strcat(concat, ageStr);
	return concat;
}

void destructPerson(Person *personPtr) {
	if(personPtr == NULL) {
		return;
	}

	free(personPtr->firstNamePtr);
	free(personPtr->lastNamePtr);
	free(personPtr->agePtr);
	free(personPtr);
}


