/**
 * @file
 * @brief Implementation of the List utils
 *
 * Doubly linked list
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P5
 */
#include <stdio.h>
#include <stdlib.h>

#include "list.h"


List *createList() {
	ListElement element = { .personPtr=NULL, .nextPtr=NULL, .prevPtr=NULL };

	int size = 0;

	ListElement *headPtr = (ListElement*) malloc(sizeof(element));
	*headPtr = element;
	headPtr->nextPtr = headPtr;
	headPtr->prevPtr = headPtr;

	List list = { .headPtr=headPtr, size=0 };
	List *listPtr = (List*) malloc(sizeof(list));
	*listPtr = list;

	return listPtr;
}

/**
 * @brief Adds a given person object after the given listElement
 *
 * @param[in] listPtr
 * @param[in] searchPtr
 * @param[in] personPtr
 */
void addToListAfterElement(List *listPtr, ListElement *searchPtr, Person *personPtr) {
	ListElement element = { .nextPtr = searchPtr->nextPtr,
							.prevPtr = searchPtr,
							.personPtr = personPtr };

	ListElement *newElemenentPtr = (ListElement*) malloc(sizeof(element));
	*newElemenentPtr = element;

	searchPtr->nextPtr->prevPtr = newElemenentPtr;
	searchPtr->nextPtr = newElemenentPtr;
	listPtr->size++;
}

void addToList(List *listPtr, Person *personPtr) {

	if(listPtr == NULL) {
		return;
	}

	ListElement *searchPtr = listPtr->headPtr;

	if(searchPtr->nextPtr == listPtr->headPtr) {
		addToListAfterElement(listPtr, searchPtr, personPtr);
		return;
	}

	int searchPtrCmp = personcmp(searchPtr->personPtr, personPtr);
	int searchPtrNextCmp = personcmp(searchPtr->nextPtr->personPtr, personPtr);

	while(searchPtrCmp < 0 && searchPtrNextCmp < 0 && searchPtr->nextPtr != listPtr->headPtr) {
		searchPtr = searchPtr->nextPtr;
		searchPtrCmp = personcmp(searchPtr->personPtr, personPtr);
		searchPtrNextCmp = personcmp(searchPtr->nextPtr->personPtr, personPtr);
	}

	addToListAfterElement(listPtr, searchPtr, personPtr);
}

/**
 * @brief Frees a list element and the person object
 *
 * @param[in] listPtr
 * @param[in] nodePtr
 */
void destructListElement(List *listPtr, ListElement *nodePtr) {
	if(listPtr == NULL) {
		return;
	}

	if (listPtr->size == 0) {
		return; // nothing to do
	}

	nodePtr->prevPtr->nextPtr = nodePtr->nextPtr;
	nodePtr->nextPtr->prevPtr = nodePtr->prevPtr;
	//destructPerson(nodePtr->personPtr);
	free(nodePtr);
	listPtr->size--;
}

void clearList(List *listPtr) {
	if(listPtr == NULL) {
		return;
	}

	if (listPtr->headPtr != listPtr->headPtr->nextPtr
			&& listPtr->size > 0) {
		destructListElement(listPtr, listPtr->headPtr->nextPtr);
		clearList(listPtr);
	}
}

void removeFromList(List *listPtr, Person *personPtr) {

	if(listPtr->size <= 0) {
		return;
	}

	ListElement *searchPtr = listPtr->headPtr->nextPtr;
	while(searchPtr != listPtr->headPtr) {
		if(personcmp(searchPtr->personPtr, personPtr) == 0) {
			destructListElement(listPtr, searchPtr);
			break;
		}
		searchPtr = searchPtr->nextPtr;
	}
}

void removeFromListByIndex(List *listPtr, int index) {
	Person *personPtr = getPersonAtIndex(listPtr, index);
	removeFromList(listPtr, personPtr);
}


void printList(List *listPtr) {
	int counter = 1;
	ListElement *currentNodePtr = listPtr->headPtr->nextPtr;
	while (currentNodePtr != listPtr->headPtr) {
		printf("%3d. %s\n", counter, personf(currentNodePtr->personPtr));
		currentNodePtr = currentNodePtr->nextPtr;
		counter++;
	}
	printf("Current Size of List %d\n\n", listPtr->size);
}

Person *getPersonAtIndex(List *listPtr, int index) {
	if(listPtr == NULL) {
		return NULL;
	}

	if(listPtr->size == 0 || index < 0 || index >= listPtr->size) {
		return NULL;
	}

	ListElement *currentPtr = listPtr->headPtr->nextPtr;

	int counter = 0;

	while(counter != index) {
		currentPtr = currentPtr->nextPtr;
		counter++;
	}

	return currentPtr->personPtr;
}
