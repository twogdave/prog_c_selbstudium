/**
 * @file
 * @brief Test suite for the given package.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CUnit/Basic.h"
#include "CUnit/CUnit.h"
#include "test_utils.h"

#include "list.h"
#include "person.h"

/**
 * @file
 * @brief Testing routines for the files main.c list.c and person.c
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P5
 */
/** @brief Runs only if TARGET1 is defined in make file */
#ifndef TARGET1
	#error missing TARGET1 define
#endif

/** @brief The name of the STDOUT text file. */
#define OUTFILE "../resources/test/output/stdout.txt"
/** @brief The name of the STDERR text file. */
#define ERRFILE "../resources/test/output/stderr.txt"
/** @brief The stimulus file for simple console input */
#define INFILE_SIMPLE "../resources/test/input/llp-simple.input"
/** @brief The stimulus file for remove console input */
#define INFILE_REMOVE "../resources/test/input/llp-remove.input"
/** @brief The stimulus file for clear console input */
#define INFILE_CLEAR "../resources/test/input/llp-clear.input"
/** @brief The stimulus file for sort console input */
#define INFILE_SORT "../resources/test/input/llp-sort.input"

/**
 * @brief setup for the test
 *
 * deletes STDOUT AND STDERR file if exists
 */
static int setup(void)
{
	remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
	return 0; // success
}

/**
 * @brief teardown for the test
 *
 * Nothing to do
 */
static int teardown(void)
{
	return 0;
}


/**
 * @brief Test simple console input
 *
 * Tests a simple usage of the cli 2 iserts, 1 show
 * and at least end the program. Compares the output with the expected
 * Rows in input-file
 */
static void testMainSimple(void)
{
	// arrange
	 const char *out_txt[] = {
			  "Enter a command.\n"
			 ,"	(i)nsert\n"
			 ,"	(r)emove\n"
			 ,"	(s)how\n"
			 ,"	(c)lear\n"
			 ,"	(e)nd\n"
			 ,"Command: Insert a new Person\n"
			 ,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			 ,"New Person inserted!\n"
			 ,"Last-Name: Giunta\n"
			 ,"First-Name: Davide\n"
			 ,"Age: 33\n"
			 ,"Showing all registered persons.\n"
			 ,"  1. Giunta Davide  33\n"
			 ,"Current Size of List 1\n"
			 ,"\n"
			 ,"\n"
			 ,"Enter a command.\n"
			 ,"	(i)nsert\n"
			 ,"	(r)emove\n"
			 ,"	(s)how\n"
			 ,"	(c)lear\n"
			 ,"	(e)nd\n"
			 ,"Command: Insert a new Person\n"
			 ,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			 ,"New Person inserted!\n"
			 ,"Last-Name: Meier\n"
			 ,"First-Name: Hans\n"
			 ,"Age: 46\n"
			 ,"Showing all registered persons.\n"
			 ,"  1. Giunta Davide  33\n"
			 ,"  2. Meier Hans  46\n"
			 ,"Current Size of List 2\n"
			 ,"\n"
			 ,"\n"
			 ,"Enter a command.\n"
			 ,"	(i)nsert\n"
			 ,"	(r)emove\n"
			 ,"	(s)how\n"
			 ,"	(c)lear\n"
			 ,"	(e)nd\n"
			 ,"Command: Showing all registered persons.\n"
			 ,"  1. Giunta Davide  33\n"
			 ,"  2. Meier Hans  46\n"
			 ,"Current Size of List 2\n"
			 ,"\n"
			 ,"\n"
			 ,"Enter a command.\n"
			 ,"	(i)nsert\n"
			 ,"	(r)emove\n"
			 ,"	(s)how\n"
			 ,"	(c)lear\n"
			 ,"	(e)nd\n"
			 ,"Command: Bye Bye!\n"
	};
	const char *err_txt[] = { };
	// act
	int exit_code = system(XSTR(TARGET1) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_SIMPLE);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
	assert_lines(ERRFILE, err_txt, sizeof(err_txt)/sizeof(*err_txt));
}

/**
 * @brief Test clear console input
 *
 * Tests a clear usage of the cli 3 new entry entered,
 * show the entrys, clear the list, show the entry
 * and end the program. Compares the output with the expected
 * Rows in input-file
 */
static void testMainClear(void)
{
	// arrange
	const char *out_txt[] = {
			 "Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Giunta\n"
			,"First-Name: Davide\n"
			,"Age: 27\n"
			,"Showing all registered persons.\n"
			,"  1. Giunta Davide  27\n"
			,"Current Size of List 1\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Maria\n"
			,"First-Name: Miria\n"
			,"Age: 25\n"
			,"Showing all registered persons.\n"
			,"  1. Giunta Davide  27\n"
			,"  2. Maria Miria  25\n"
			,"Current Size of List 2\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Simon\n"
			,"First-Name: Stocker\n"
			,"Age: 25\n"
			,"Showing all registered persons.\n"
			,"  1. Giunta Davide  27\n"
			,"  2. Maria Miria  25\n"
			,"  3. Simon Stocker  25\n"
			,"Current Size of List 3\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Showing all registered persons.\n"
			,"  1. Giunta Davide  27\n"
			,"  2. Maria Miria  25\n"
			,"  3. Simon Stocker  25\n"
			,"Current Size of List 3\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: \n"
			,"Deleting all the entries...\n"
			,"Showing all registered persons.\n"
			,"Current Size of List 0\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Showing all registered persons.\n"
			,"Current Size of List 0\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Bye Bye!\n"
	};
	const char *err_txt[] = { };
	// act
	int exit_code = system(XSTR(TARGET1) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_CLEAR);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
	assert_lines(ERRFILE, err_txt, sizeof(err_txt)/sizeof(*err_txt));
}

/**
 * @brief Test sort console input
 *
 * Tests a clear usage of the cli 3 nearly identicaly new entry entered,
 * show the entrys, end the program. Compares the output with the expected
 * Rows in input-file
 */
static void testMainSort(void)
{
	// arrange
	const char *out_txt[] = {
			 "Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Peter\n"
			,"First-Name: Mueller\n"
			,"Age: 50\n"
			,"Showing all registered persons.\n"
			,"  1. Peter Mueller  50\n"
			,"Current Size of List 1\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Max\n"
			,"First-Name: Mueller\n"
			,"Age: 40\n"
			,"Showing all registered persons.\n"
			,"  1. Max Mueller  40\n"
			,"  2. Peter Mueller  50\n"
			,"Current Size of List 2\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Peter\n"
			,"First-Name: Mueller\n"
			,"Age: 30\n"
			,"Showing all registered persons.\n"
			,"  1. Max Mueller  40\n"
			,"  2. Peter Mueller  30\n"
			,"  3. Peter Mueller  50\n"
			,"Current Size of List 3\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Showing all registered persons.\n"
			,"  1. Max Mueller  40\n"
			,"  2. Peter Mueller  30\n"
			,"  3. Peter Mueller  50\n"
			,"Current Size of List 3\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Bye Bye!\n"
	};

	const char *err_txt[] = { };
	// act
	int exit_code = system(XSTR(TARGET1) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_SORT);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
	assert_lines(ERRFILE, err_txt, sizeof(err_txt)/sizeof(*err_txt));
}

/**
 * @brief Test remove console input
 *
 * Tests a remove usage of the cli 3 new entry entered,
 * 2 gets removed, 1 should be remain. Compares the output with the expected
 * Rows in input-file
 */
static void testMainRemove(void)
{
	// arrange
	const char *out_txt[] = {
			 "Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Giunta\n"
			,"First-Name: Davide\n"
			,"Age: 33\n"
			,"Showing all registered persons.\n"
			,"  1. Giunta Davide  33\n"
			,"Current Size of List 1\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Andreas\n"
			,"First-Name: Losli\n"
			,"Age: 25\n"
			,"Showing all registered persons.\n"
			,"  1. Andreas Losli  25\n"
			,"  2. Giunta Davide  33\n"
			,"Current Size of List 2\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Insert a new Person\n"
			,"Last name (max 50 chars): First name (max 50 chars): Age: \n"
			,"New Person inserted!\n"
			,"Last-Name: Test\n"
			,"First-Name: Test\n"
			,"Age: 12\n"
			,"Showing all registered persons.\n"
			,"  1. Andreas Losli  25\n"
			,"  2. Giunta Davide  33\n"
			,"  3. Test Test  12\n"
			,"Current Size of List 3\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Showing all registered persons.\n"
			,"  1. Andreas Losli  25\n"
			,"  2. Giunta Davide  33\n"
			,"  3. Test Test  12\n"
			,"Current Size of List 3\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: \n"
			,"Select the entry index you want to delete\n"
			,"  1. Andreas Losli  25\n"
			,"  2. Giunta Davide  33\n"
			,"  3. Test Test  12\n"
			,"Current Size of List 3\n"
			,"\n"
			,"Index to delete: \n"
			,"Deleting index 2\n"
			,"Showing all registered persons.\n"
			,"  1. Andreas Losli  25\n"
			,"  2. Test Test  12\n"
			,"Current Size of List 2\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: \n"
			,"Select the entry index you want to delete\n"
			,"  1. Andreas Losli  25\n"
			,"  2. Test Test  12\n"
			,"Current Size of List 2\n"
			,"\n"
			,"Index to delete: \n"
			,"Deleting index 1\n"
			,"Showing all registered persons.\n"
			,"  1. Test Test  12\n"
			,"Current Size of List 1\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Showing all registered persons.\n"
			,"  1. Test Test  12\n"
			,"Current Size of List 1\n"
			,"\n"
			,"\n"
			,"Enter a command.\n"
			,"	(i)nsert\n"
			,"	(r)emove\n"
			,"	(s)how\n"
			,"	(c)lear\n"
			,"	(e)nd\n"
			,"Command: Bye Bye!\n"
	};

	const char *err_txt[] = { };
	// act
	int exit_code = system(XSTR(TARGET1) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_REMOVE);
	// assert
	CU_ASSERT_EQUAL(exit_code, 0);
	assert_lines(OUTFILE, out_txt, sizeof(out_txt)/sizeof(*out_txt));
	assert_lines(ERRFILE, err_txt, sizeof(err_txt)/sizeof(*err_txt));
}

/**
 * @brief Test the behaviour of the linked list console input
 */
void testLinkedList(void) {
	List *listPtr = createList();
	Person *person = createPerson("Giunta", "Nicole", 32);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 1);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 0)), "Giunta Nicole  32") == 0);

	person = createPerson("Weidman", "Bruno", 43);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 2);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 1)), "Weidman Bruno  43") == 0);

	person = createPerson("Giunta", "Davide", 33);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 3);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 0)), "Giunta Davide  33") == 0);

    person = createPerson("Hermann", "Karl", 72);
    addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 4);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 2)), "Hermann Karl  72") == 0);

	person = createPerson("Calabrese", "Santa", 64);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 5);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 0)), "Calabrese Santa  64") == 0);

	person = createPerson("Giunta", "Davide", 43);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 6);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 2)), "Giunta Davide  43") == 0);

	person = createPerson("Giunta", "Davide", 23);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 7);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 1)), "Giunta Davide  23") == 0);

	person = createPerson("Giunta", "Davide", -1);
	addToList(listPtr, person);
	CU_ASSERT_EQUAL(listPtr->size, 8);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 1)), "Giunta Davide  -1") == 0);

	removeFromList(listPtr, listPtr->headPtr->nextPtr->nextPtr->personPtr); // remove Giunta Davide -1
	removeFromList(listPtr, listPtr->headPtr->prevPtr->personPtr); // remove Weidman Bruno 43
	removeFromList(listPtr, listPtr->headPtr->prevPtr->prevPtr->personPtr); // remove Giunta Nicole 32

	CU_ASSERT_EQUAL(listPtr->size, 5);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 0)), "Calabrese Santa  64") == 0);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 1)), "Giunta Davide  23") == 0);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 2)), "Giunta Davide  33") == 0);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 3)), "Giunta Davide  43") == 0);
	CU_ASSERT_TRUE(strcmp(personf(getPersonAtIndex(listPtr, 4)), "Hermann Karl  72") == 0);

	clearList(listPtr);
	CU_ASSERT_EQUAL(listPtr->size, 0);
	CU_ASSERT_TRUE(listPtr->headPtr == listPtr->headPtr->nextPtr);
}

/**
 * @brief Test the behaviour of the person
 */
void testPerson() {
		Person *person01 = createPerson("Dell Piero", "Allesandro", 46);
		CU_ASSERT_TRUE(strcmp(personf(person01), "Dell Piero Allesandro  46") == 0)

		Person *person02 = createPerson("Dell Piero", "Allesandro", 46);
		CU_ASSERT_EQUAL(personcmp(person01, person02), 0);

		Person *person03 = createPerson("Dell Piero", "Allesandro", 23);
		CU_ASSERT_EQUAL(personcmp(person01, person03), 1);
		CU_ASSERT_EQUAL(personcmp(person03, person01), -1);
}

/**
 * @brief Registers and runs the tests.
 *
 * @return EXIST_SUCCESS
 */
int main(void)
{
	// setup, run, teardown
	TestMainBasic("PROGC_C_P5", setup, teardown
				  , testPerson
				  , testLinkedList
				  , testMainSimple
				  , testMainClear
                  , testMainRemove
                  , testMainSort
				  );
}
