#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CUnit/Basic.h"
#include "CUnit/CUnit.h"
#include "test_utils.h"

#include "wordCount.h"

/**
 * @file
 * @brief Testing routines for the file wordCount.c
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P4
 */

/** @brief Runs only if TARGET1 is defined in make file */
#ifndef TARGET1
	#error TARGET1 is not definded
#endif

/**
 * @brief setup for the test
 *
 * Nothing to do
 */
static int setup(void)
{
 	return EXIT_SUCCESS;
}
/**
 * @brief teardown for the test
 *
 * Nothing to do
 */
static int teardown(void)
{
	return EXIT_SUCCESS;
}

/**
 * @brief Test Add Words
 *
 * Test adding words to the Wordcounter
 * Adds the words "Wort-1", "Wort-2", "Wort-3", "Wort-4", "Wort-5"
 * and checks if they have been stored int to wordArray
 */
void testAddWords() {
	WordCounter wordCounterAddWords = construct();

	int i;
	char wordToAdd[7];
	for(i = 1; i <= 5; i++) {
		sprintf(wordToAdd, "Wort-%d", i);
		addWord(wordToAdd, &wordCounterAddWords);
	}

	for(i = 0; i < wordCounterAddWords.wordCount; i++) {
		sprintf(wordToAdd, "Wort-%d", (i+1));
		int strcmpResult = strcmp(wordCounterAddWords.wordList[i], wordToAdd);
		CU_ASSERT_EQUAL(strcmpResult, 0);
	}
}

/**
 * @brief Test Sort Words
 *
 * Test adding words to the Wordcounter and sorting it
 * Adds the words "Wort-1", "Wort-12", "Wort-3", "Wort-14", "Wort-5"
 * "Wort-16", "Wort-7", "Wort-18", "Wort-9", "Wort-20"
 * after sorting it, it checks if they have correct sorted
 * like "Wort-1", "Wort-12", "Wort-14", "Wort-16", "Wort-18", "Wort-20",
 * "Wort-3", "Wort-5", "Wort-7", "Wort-9"
 */
void testSortWords() {
	WordCounter wordCounterSortWords = construct();

	// 1, 12, 3, 14, 5, 16, 7, 18, 9, 20
	int i, j, k;
	char wordToAdd[8];
	for(i = 1, j = 11; i <= 10; i++, j++) {
		k = i % 2 == 0 ? j : i;
		sprintf(wordToAdd, "Wort-%d", k);
		addWord(wordToAdd, &wordCounterSortWords);
	}

	sortWords(&wordCounterSortWords, false);

	// the alphabetical order
	int exptected[] = { 1, 12, 14, 16, 18, 20, 3, 5, 7, 9 };

	for(i = 0 ; i < wordCounterSortWords.wordCount; i++) {
		sprintf(wordToAdd, "Wort-%d", exptected[i]);
		int strcmpResult = strcmp(wordCounterSortWords.wordList[i], wordToAdd);
		CU_ASSERT_EQUAL(strcmpResult, 0);
	}
}

/**
 * @brief Registers and runs the tests.
 *
 * @return EXIT_SUCCES
 */
int main(void)
{
	TestMainBasic("ProgC P4 - Add Words and Sort", setup, teardown
				  , testAddWords
				  , testSortWords
				  );

	return EXIT_SUCCESS;
}
