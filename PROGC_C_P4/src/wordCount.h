/**
 * @file
 * @brief Word Count and Sort header file
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P4
 */
#ifndef WORD_COUNT_H_
#define WORD_COUNT_H_

	#include <stdbool.h>

	/** @brief makro that returns the size of an array */
	#define SIZE_OF_ARRAY(x)  (sizeof(x) / sizeof((x)[0]))

	/** @brief WordCounter struct */
	typedef struct {
		/** @brief wordList array of char pointer of len 100 */
		char *wordList[100];
		/** @brief len of wordList array */
		int wordCount;
		/** @brief boolean that shows if the list has been sorted */
		bool sortNeeded;
		/** @brief sort order asc (=false) or desc (=true)  */
		bool sortOrderDesc;
	} WordCounter;

	/**
	 * @brief Constructs a WordCounter object
	 *
	 * @return WordCounter
	 */
	WordCounter construct();

	/**
	 * @brief adds a Word to the WordCounter
	 *
	 * @param[in] *wordToAddPtr
	 * @param[in] *wordCounterPtr
	 */
	void addWord(char *wordToAddPtr, WordCounter *wordCounterPtr);

	/**
	 * @brief Sort words
	 *
	 * @param[in] *wordCounterPtr
	 * @param[in] desc
	 */
	void sortWords(WordCounter *wordCounterPtr, bool desc);

#endif /* WORD_COUNT_H_ */
