#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "wordCount.h"

/**
 * @file
 * @brief Implementation of the WordCounter functions
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P4
 */

WordCounter construct() {
	WordCounter counter = { .wordCount = 0, .sortNeeded = true, .sortOrderDesc = false };
	return counter;
}

void addWord(char* wordToAddPtr, WordCounter* wordCounterPtr){

		char *newWordPtr = (char*) malloc(sizeof(*wordToAddPtr));
		strcpy(newWordPtr, wordToAddPtr);

		if(wordCounterPtr->wordCount + 1 < SIZE_OF_ARRAY(wordCounterPtr->wordList)) {
			wordCounterPtr->wordList[wordCounterPtr->wordCount] = newWordPtr;
			wordCounterPtr->wordCount++;
			wordCounterPtr->sortNeeded = true;
		}
}

/**
 * @brief bubblesort of an array of pointer
 *
 * @param[in] arrayOfPtr[]
 * @param[in] length
 * @param[in] desc
 */
void bubblesort(char* arrayOfPtr[], int length, bool desc) {
   char* tmp;
   int i, j;
   for (i = 0; i < length ; i++) {
      for (j = i + 1; j < length ; j++) {

    	  int compareResult = !desc ? strcasecmp(arrayOfPtr[i], arrayOfPtr[j]) : strcasecmp(arrayOfPtr[j], arrayOfPtr[i]);

          if (compareResult > 0) {
              tmp = arrayOfPtr[i];
              arrayOfPtr[i] = arrayOfPtr[j];
              arrayOfPtr[j] = tmp;
          }
      }
   }
}

void sortWords(WordCounter* wordCounterPtr, bool desc) {
	bubblesort(wordCounterPtr->wordList, wordCounterPtr->wordCount, desc);
	wordCounterPtr->sortNeeded = false;
	wordCounterPtr->sortOrderDesc = desc;
}

/**
 * @brief welcomes the user
 */
void welcome(){
	printf("Woerter-Z�hlen!\n");
	printf("Sie k�nnen bis zu 100 Wierter eingeben und das Programm wird diese anschliessend alphabetisch sortieren und zaehlen\n");
	printf("Beenden Sie ihre eingabe mit ZZZ\n");
	fflush(stdout);
}
/**
 * @brief prints the result to the console
 *
 * first asc and then desc order
 *
 * @param[in] wordCounterPtr
 */
void printResult(WordCounter* wordCounterPtr) {

	printf("Woerter sortiert (%s):\n", wordCounterPtr->sortOrderDesc ? "absteigend" : "aufsteigend");
	int i;

	for(i = 0; i < wordCounterPtr->wordCount; i++) {
		printf("%s\n", wordCounterPtr->wordList[i]);
	}

	printf("\n");
	printf("Woerter Total: %d\n", wordCounterPtr->wordCount);

}
/**
 * @brief Reads words from the Console counts and sorts it
 */
void run() {
	welcome();

	WordCounter wordCounter = construct();

	char wordToRead[100] = "";
	char stopWord[] = "ZZZ";

	while(strcmp(wordToRead, stopWord) != 0) {
		scanf("%s", wordToRead);
		if(strcmp(wordToRead, stopWord) != 0) {
			addWord(wordToRead, &wordCounter);
		}
	}

	sortWords(&wordCounter, false);
	printResult(&wordCounter);

	sortWords(&wordCounter, true);
	printResult(&wordCounter);
}

/**
 * @brief Main run method
 *
 * @return EXIT_SUCCESS
 */
int main(void) {
	run();
	return EXIT_SUCCESS;
}
