/**
 * @file
 * @brief Impementation of markStatistic.h
 *
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P3
 */

#ifndef MARK_STATISTIC_H_
#define MARK_STATISTIC_H_

	#include <stdbool.h>

	/** @brief makro that returns the size of an array */
	#define SIZE_OF_ARRAY(x)  (sizeof(x) / sizeof((x)[0]))

	/** @brief Markevaluatro struct, contains all relevant calculation data and the result */
	typedef struct {
		/** @brief length of the pointsArray */
		int len;
		/** @brief max points neede for the best mark */
		int maxPoints;
		/** @brief array that contains all the points per student */
		int pointsArray[100];
		/** @brief evaluation array containing the counts per mark lenght = 6 */
		int markCounterArray[6];
		/** @brief calculated average */
		float averageMark;
		/** @brief best mark calculated */
		int bestMark;
		/** @brief worst mark calculated */
		int worstMark;
		/** @brief counter for the students that passed the test */
		int pastCounter;
		/** @brief percentage of past students */
		float pastInPercent;
		/** @brief boolean that shows if a calculation has been allready made */
		bool calculationNeeded;
	} MarkEvaluator;

	/**
	 * @brief Creates a markevaluator
	 *
	 * @param[in] maxPoints
	 * @return Markevaluator
	 */
	MarkEvaluator construct(int maxPoints);

	/**
	 * @brief calculates the statistic
	 *
	 * @param[in] markEvaluatorPtr
	 */
	void calculateStatistic(MarkEvaluator *markEvaluatorPtr);

	/**
	 * @brief add point to Markevaluatro pointerArray
	 *
	 * @param[in] pointsPtr
	 * @param[in] markEvaluatorPtr
	 * @return the given pointsPtr
	 */
	int *addPoints(int *pointsPtr, MarkEvaluator *markEvaluatorPtr);

	/**
	 * @brief Removes points at a given position in pointerArray of Markevaluator
	 *
	 * @param[in] position
	 * @param[in] markEvaluatorPtr
	 *
	 * @return pointer to the removed value from array
	 */
	int *removePoints(int position, MarkEvaluator *markEvaluatorPtr);

	/**
	 * @brief reads a int value from the console
	 *
	 *
	 * @param[in] markEvaluatorPtr
	 * @return inserted value pointer
	 */
	int* readPoints(MarkEvaluator* markEvaluatorPtr);

	/**
	 * @brief prints the result statistic to the console
	 *
	 * @param[in] markEvaluatorPtr
	 */
	void printMarkStatistic(MarkEvaluator* markEvaluatorPtr);

#endif /* MARK_STATISTIC_H_ */
