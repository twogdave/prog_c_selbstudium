#include "markStatistic.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/**
 * @file
 * @brief Impementation of markStatistic.h
 *
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P3
 */
MarkEvaluator construct(int maxPoints){
	MarkEvaluator markEvaluator = { .len = 0,
									.maxPoints = maxPoints,
									.markCounterArray = { 0, 0, 0, 0, 0, 0 },
									.averageMark = 0,
									.bestMark = 0,
									.worstMark = 0,
									.pastCounter = 0,
									.pastInPercent = 0.0,
									.calculationNeeded = true
									};

	return markEvaluator;
}

void calculateStatistic(MarkEvaluator* markEvaluatorPtr) {

	if(!markEvaluatorPtr->calculationNeeded) {
		return;
	}

	markEvaluatorPtr->averageMark = 0;

	int i;
	for(i = 0; i < markEvaluatorPtr->len; i++){
		// plus 1.5 because you need to add one from the formula ((Points * 5)/MaxPoints)+1
		// and if you add 0.5 to the result of the formula and cast it to an int it becomes rounded to integer number
		// Example: 5.5 + 0.5 = 6.0 --> in int --> 6
		// 		    5.1 + 0.5 = 5.6 --> in int --> 5
		//          5.7 + 0.5 = 6.2 --> in int --> 6 and so on...
		int mark = ((float) markEvaluatorPtr->pointsArray[i] * 5.0) / (float) markEvaluatorPtr->maxPoints + 1.5;
		markEvaluatorPtr->markCounterArray[mark - 1] = markEvaluatorPtr->markCounterArray[mark - 1] + 1;
		markEvaluatorPtr->averageMark += mark;
		markEvaluatorPtr->bestMark = markEvaluatorPtr->bestMark < mark ? mark : markEvaluatorPtr->bestMark;
		markEvaluatorPtr->worstMark = markEvaluatorPtr->worstMark > mark || markEvaluatorPtr->worstMark == 0 ? mark : markEvaluatorPtr->worstMark;
		markEvaluatorPtr->pastCounter += mark >= 4 ? 1 : 0;
		markEvaluatorPtr->pastInPercent = 100.0 / ((float) markEvaluatorPtr->len) * ((float) markEvaluatorPtr->pastCounter);
	}

	markEvaluatorPtr->averageMark = markEvaluatorPtr->averageMark /markEvaluatorPtr->len;

}

int* addPoints(int* pointsPtr, MarkEvaluator* markEvaluatorPtr){

	if(*pointsPtr < 0 || *pointsPtr > markEvaluatorPtr->maxPoints) {
		return pointsPtr;
	}

	markEvaluatorPtr->calculationNeeded = true;

	if(markEvaluatorPtr->len < SIZE_OF_ARRAY(markEvaluatorPtr->pointsArray)) {
		markEvaluatorPtr->pointsArray[markEvaluatorPtr->len] = *pointsPtr;
		markEvaluatorPtr->len = markEvaluatorPtr->len + 1;
	}

	return pointsPtr;
}

int* removePoints(int position, MarkEvaluator* markEvaluatorPtr){

	int* newPositionPtr;
	if(position >= markEvaluatorPtr->len) {
		newPositionPtr = &markEvaluatorPtr->pointsArray[position];
		return newPositionPtr;
	}

	markEvaluatorPtr->calculationNeeded = true;

	int i;
	for(i = position; i < markEvaluatorPtr->len; i++) {
		markEvaluatorPtr->pointsArray[i] = markEvaluatorPtr->pointsArray[i + 1];
	}

	markEvaluatorPtr->pointsArray[i] = 0;
	markEvaluatorPtr->len--;

	newPositionPtr = &markEvaluatorPtr->pointsArray[position];
	return newPositionPtr;
}

int* readPoints(MarkEvaluator *markEvaluatorPtr){

	MarkEvaluator eval = *markEvaluatorPtr;

	int newPoints;
	printf("Punkte eingeben: ");
	fflush(stdout);
	scanf("%d", &newPoints);

	printf("newPoints=%d\n", newPoints); // TODO remove me
	printf("markEvaluator.maxPoints=%d\n", eval.maxPoints); // TODO remove me

	while(newPoints < -1 || newPoints > eval.maxPoints) {
		printf("Die Punkte-Anzahl die Sie eingegeben haben ist nicht g�ltig!\n");
		printf("Bitte geben Sie eine Punkte-Anzahl zwischen 1 und %d an. (Geben Sie -1 um abzubrechen)\n", eval.maxPoints);
		printf("Punkte eingeben: ");
		fflush(stdout);
		scanf("%d", &newPoints);
	}

	int* newPointsPtr;
	if(newPoints != -1) {
		newPointsPtr = addPoints(&newPoints, markEvaluatorPtr);
	}

	return newPointsPtr;
}

void printMarkStatistic(MarkEvaluator* markEvaluatorPtr){

	if(markEvaluatorPtr->len < 1) {
		printf("Keine Daten f�r die Evaluierung vorhanden");
		return;
	}

	char horizontalLine[68];
	int i;
	for(i = 0; i < SIZE_OF_ARRAY(horizontalLine) - 1; i++) {
		horizontalLine[i] = '-';
	}
	horizontalLine[SIZE_OF_ARRAY(horizontalLine) - 1] = '\0';

	printf("%s\n", horizontalLine);
	printf("Statistik (%d Studenten, min. %d Punkte werden ben�tigt f�r eine 6)\n", markEvaluatorPtr->len, markEvaluatorPtr->maxPoints);

	for(i = 0; i < SIZE_OF_ARRAY(markEvaluatorPtr->markCounterArray); i++) {
		printf("Note %d: %d\n", i + 1, markEvaluatorPtr->markCounterArray[i]);
	}

	printf("Beste Note: %d\n", markEvaluatorPtr->bestMark);
	printf("Schlechteste Note: %d\n", markEvaluatorPtr->worstMark);
	printf("Durchschnitt Note: %1.2f\n", markEvaluatorPtr->averageMark);
	printf("Note >= 4: %d Studenten (%3.2f%%)\n", markEvaluatorPtr->pastCounter, markEvaluatorPtr->pastInPercent);
	printf("%s\n", horizontalLine);
}

/**
 * @brief run metho called from main
 */
void runEvaluator() {

	printf("Willkommmen zum Noten-Evaluations-Tool!\n");
	printf("Bitte geben Sie die Maximal-Punkzahl f�r die Note 6 ein: ");
	fflush(stdout);
	int maxPoints = 0;
	scanf("%d", &maxPoints);

	MarkEvaluator markEvalutor = construct(maxPoints);
	int result = *readPoints(&markEvalutor);
	while(result != -1) {
		result = *readPoints(&markEvalutor);
	}

	calculateStatistic(&markEvalutor);
	printMarkStatistic(&markEvalutor);
}

/**
 * @brief Main function to start the program
 *
 * @return EXIT_SUCCES
 */
int main(void) {
	runEvaluator();
	return EXIT_SUCCESS;
}
