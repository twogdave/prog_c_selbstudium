#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CUnit/Basic.h"
#include "CUnit/CUnit.h"
#include "test_utils.h"

#include "markStatistic.h"

/**
 * @file
 * @brief Testing routines to the the file dateUtil.c
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P1 - Aufgabe 2
 */

/** @brief Runs only if TARGET2 is defined in make file */
#ifndef TARGET1 // must be given by the make file --> see test target
	#error TARGET1 is not definded
#endif



/**
 * @brief setup for the test
 *
 * nothing to do
 */
static int setup(void)
{
 	return 0;
}
/**
 * @brief teardown for the test
 *
 * Nothing to do
 */
static int teardown(void)
{
	// nothing todo.
	return 0;
}

/**
 * @brief Main Test of MarkStatistic
 *
 * Test of all the functionality of the Mark Statistic
 */
static void testCalculateStatistic() {
	int maxPoints = 12;
	MarkEvaluator me = construct(maxPoints);

	int pointsArr[14] = { 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, -1 };
	int i;
	for(i = 0; i < SIZE_OF_ARRAY(pointsArr); i++) {
		addPoints(&pointsArr[i], &me);
	}

	calculateStatistic(&me);

	CU_ASSERT_EQUAL(me.markCounterArray[0], 2);
	CU_ASSERT_EQUAL(me.markCounterArray[1], 2);
	CU_ASSERT_EQUAL(me.markCounterArray[2], 2);
	CU_ASSERT_EQUAL(me.markCounterArray[3], 3);
	CU_ASSERT_EQUAL(me.markCounterArray[4], 2);
	CU_ASSERT_EQUAL(me.markCounterArray[5], 2);

	CU_ASSERT_EQUAL(me.len, 13);

	CU_ASSERT_EQUAL(me.bestMark, 6);
	CU_ASSERT_EQUAL(me.worstMark, 1);
}

/**
 * @brief Registers and runs the tests.
 *
 * @return EXIT_SUCCES
 */
int main(void)
{
	TestMainBasic("ProgC P3 - Mark Statistic Tests", setup, teardown
				  , testCalculateStatistic
				  );

	return 0;
}
