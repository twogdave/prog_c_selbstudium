#include "dateUtil.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @file
 * @brief Impementation of dateutil.h
 *
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P2
 */
Date construct(int *year, int *month, int *day) {

	Date newStruct = { .year=*year, .month=*month, .day=*day };

	if(!isValidDate(newStruct)) {
		newStruct.year = 0;
		newStruct.month = 0;
		newStruct.day = 0;
	}

	return newStruct;
}

bool isLeapyear(Date date) {

	if (date.year % 4 != 0 || (date.year % 100 == 0 && date.year % 400 != 0)) {
		return false;
	}

	return true;
}

bool isValidDate(Date date) {

	Date *datePointer = &date;
	if(!datePointer || !(datePointer->day) || !(datePointer->month) || !(datePointer->year)) {
		return false;
	}

	if (date.year < 0) {
		return false;
	}

	if(date.month < 1 || date.month > 12) {
		return false;
	}

	if(date.day < 0) {
		return false;
	}

	int maxMonthDays = getMonthMaxDays(date);
	if(date.day > maxMonthDays) {
		return false;
	}

	return true;
}

int getMonthMaxDays(Date date) {
	int result = 0;

	switch (date.month) {
	case Jan:
	case Mar:
	case May:
	case Jul:
	case Aug:
	case Oct:
	case Dec:
		result += 31;
		break;
	case Feb:
		result += 28;
		if (isLeapyear(date)) {
			result++;
		}
		break;
	default:
		result += 30;
		break;
	}

	return result;
}

int countDaysFromBeginningOfYear(Date date) {
	Date counterDate = { .year=date.year, .month=1, .day=date.day };
	int result = 0;

	while (counterDate.month < date.month) {
		result += getMonthMaxDays(counterDate);
		counterDate.month++;
	}

	result += date.day;
	return result;
}

int convertToDays(Date date) {

	Date counterDate = { .year=1, .month=12, .day=31 };

	int result = 0;
	int tempDays;
	while(counterDate.year < date.year) {
		tempDays = countDaysFromBeginningOfYear(counterDate);
		result += tempDays;
		counterDate.year++;
	}

	tempDays = countDaysFromBeginningOfYear(date);
	result += tempDays;

	return result;
}

Date convertToDate(int days) {

	Date resultDate = { .year = 1, .month = 1, .day = 1 };
	for (;days > 365; days -= 365) {
		resultDate.year++;
		if(isLeapyear(resultDate)) {
			--days;
		}
	}


	if (isLeapyear(resultDate) && days == 31 + 29) {
		resultDate.month = Feb;
		resultDate.day = 29;
		return resultDate;
	}

	if (days <= 31) {
		resultDate.month = Jan;
		resultDate.day = days;
	} else if (days <= 59) {
		resultDate.month = Feb;
		resultDate.day = days - 31;
	} else if (days <= 90) {
		resultDate.month = Mar;
		resultDate.day = days - 59;
	} else if (days <= 120) {
		resultDate.month = Apr;
		resultDate.day = days - 90;
	} else if (days <= 151) {
		resultDate.month = May;
		resultDate.day = days - 120;
	} else if (days <= 181) {
		resultDate.month = Jun;
		resultDate.day = days - 151;
	} else if (days <= 212) {
		resultDate.month = Jul;
		resultDate.day = days - 181;
	} else if (days <= 243) {
		resultDate.month = Aug;
		resultDate.day = days - 212;
	} else if (days <= 273) {
		resultDate.month = Sep;
		resultDate.day = days - 243;
	} else if (days <= 304) {
		resultDate.month = Oct;
		resultDate.day = days - 273;
	} else if (days <= 334) {
		resultDate.month = Nov;
		resultDate.day = days - 304;
	} else if (days <= 365) {
		resultDate.month = Dec;
		resultDate.day = days - 334;
	}
	return resultDate;
}

Date addDay(Date date, int days) {

	if (!isValidDate(date)) {
		return date;
	}

	int totalDays = convertToDays(date) + days;
	Date resultDate = convertToDate(totalDays);

	return resultDate;
}

void printDate(Date date) {
	printf("%04d-%02d-%02d\n", date.year, date.month, date.day);
}

/**
 * Test utility to read a Date and the amount of days to add from the console
 */
void run() {

	int daysToAdd;
	int year, month, day;
	printf("Geben Sie ein Datum ein (Format yyyy-mm-dd): ");
	fflush(stdout);
	scanf("%d-%d-%d", &year, &month, &day);

	Date inpDate = construct(&year, &month, &day);
	if (!isValidDate(inpDate)) {
		printf("Das von Ihnen eingegebene Datum ist nicht gueltig!\n");
	} else {
		printf("Anzahl Tage die Sie dazu addieren moechten: ");
		// without this the output will only be send to the console at end of the Program
		fflush(stdout);
		scanf("%d", &daysToAdd);
		printf("Resultat: ");
		printDate(addDay(inpDate, daysToAdd));
	}
}

/**
 * @brief Main function to start the program
 *
 * @return EXIT_SUCCES
 */
int main(void) {
	run();
	return EXIT_SUCCESS;
}
