/**
 * @file
 * @brief Utility functions and struct definitions to manipulate a date
 *
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P2
 */
#ifndef DATE_UTIL_H_
#define DATE_UTIL_H_

	#include <stdbool.h>

	/**
	 * @brief Struct that represents a Date (format int day, int month, int year)
	 */
	typedef struct {
		/** @brief a day as int*/
		int day;
		/** @brief a month as int */
		int month;
		/** @brief a year as int */
		int year;
	} Date;

	/**
	 * @brief Enumarator for months
	 */
	enum {
		Jan = 1,
		Feb = 2,
		Mar = 3,
		Apr = 4,
		May = 5,
		Jun = 6,
		Jul = 7,
		Aug = 8,
		Sep = 9,
		Oct = 10,
		Nov = 11,
		Dec = 12
	};

	/**
	 * @brief Constructor for a Date struct
	 *
	 * if date format is not valid Date, a zero Date (0, 0, 0) will be returned
	 *
	 * @param[in] *year
	 * @param[in] *month
	 * @param[in] *day
	 * @return Date struct
	 */
	Date construct(int *year, int *month, int *day);

	/**
	 * @brief Checks if the given Year is a leap year
	 *
	 * @param[in] date - a Date struct
	 * @return true if is valid, else false
	 */
	bool isLeapyear(Date date);

	/**
	 * @brief Checks if it is a valid Date struct
	 * @param[in] date - a Date struct
	 * @return true if its valid, false if year < 0 or month < 1 or month > 12 or day < 1 or day > getMonthMaxDays
	 */
	bool isValidDate(Date date);

	/**
	 * @brief Return the maximal days that the given Date in his month can have, leap years are evaluated
	 *
	 * @param[in] date - a Date struct
	 * @return int the max days for the month
	 */
	int getMonthMaxDays(Date date);

	/**
	 * @brief Counts the days from beginning of the year
	 *
	 * @param[in] date - a Date struct
	 * @return int days
	 */
	int countDaysFromBeginningOfYear(Date date);

	/**
	 * @brief Converts a given Date struct to the amount of days beginning from Date(0, 0, 0)
	 *
	 * @param[in] date - a Date struct
	 * @returns int days
	 */
	int convertToDays(Date date);

	/**
	 * @brief Converts a amount of days to a date beginning from Date(0, 0, 0)
	 *
	 * @param[in] days
	 * @returns Date struct
	 */
	Date convertToDate(int days);

	/**
	 * @brief Adds a amount of days to a given Date struct
	 *
	 * @param[in] date - a Date struct - Date struct
	 * @param[in] days - Days to add
	 * @returns Date date
	 */
	Date addDay(Date date, int days);

	/**
	 * @brief Prints a given Date struct to the console (format %04d-%02d-%02d)
	 *
	 * @param[in] date - a Date struct
	 */
	void printDate(Date date);

#endif /* DATE_UTIL_H_ */
