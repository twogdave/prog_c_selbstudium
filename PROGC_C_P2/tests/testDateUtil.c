#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CUnit/Basic.h"
#include "CUnit/CUnit.h"
#include "test_utils.h"

#include "dateUtil.h"

/**
 * @file
 * @brief Testing routines to the the file dateUtil.c
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P1 - Aufgabe 2
 */

/** @brief Runs only if TARGET2 is defined in make file */
#ifndef TARGET1 // must be given by the make file --> see test target
	#error TARGET1 is not definded
#endif

/** @brief helper variable to create the test values */
Date date;

/** @brief helper variable to create the test values */
int day;
/** @brief helper variable to create the test values */
int month;
/** @brief helper variable to create the test values */
int year;


/**
 * @brief setup for the test
 *
 * nothing to do
 */
static int setup(void)
{
 	return 0;
}
/**
 * @brief teardown for the test
 *
 * Nothing to do
 */
static int teardown(void)
{
	// nothing todo.
	return 0;
}

/**
 * @brief Test if a date is a leap year
 *
 * Given Dates that are not leap years (ASSERT_FALSE):
 * 2.3.2014, 28.2.2018, 19.7.1689,28.2.2018, 19.7.1689, 28.2.1000
 *
 * Given Dates that are leap years (ASSERT_TRUE):
 * 15.12.1616, 19.5.1824, 29.2.2024
 */
static void testIsLeapYear(void)
{
	day=2;
	month=Mar;
	year=2014;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isLeapyear(date));

	day=28;
	month=Feb;
	year=2018;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isLeapyear(date));

	day=19;
	month=Jun;
	year=1689;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isLeapyear(date));

	day=28;
	month=Feb;
	year=2018;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isLeapyear(date));

	day=19;
	month=Jun;
	year=1689;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isLeapyear(date));

	day=28;
	month=Feb;
	year=1000;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isLeapyear(date));

	day=15;
	month=Dec;
	year=1616;
	date = construct(&year, &month, &day);
	CU_ASSERT_TRUE(isLeapyear(date));

	day=19;
	month=May;
	year=1824;
	date = construct(&year, &month, &day);
	CU_ASSERT_TRUE(isLeapyear(date));

	day=29;
	month=Feb;
	year=2024;
	date = construct(&year, &month, &day);
	CU_ASSERT_TRUE(isLeapyear(date));
}
/**
 * @brief Test if a date is valid
 *
 * Given Dates that are not valid (ASSERT_FALSE):
 * 29.2.2023, 34.11.1985, -4.5.1985, 31.4.1985, 0.0.0, 24.5.-35
 * 23.-1.1985,
 *
 * Given Dates that are leap years (ASSERT_TRUE):
 * 23.4.1985, 31.10.1985, 1.1.1
 */
static void testIsValidDate(void) {
	day=29;
	month=Feb;
	year=2023;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=34;
	month=Nov;
	year=1985;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=-4;
	month=May;
	year=1985;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=31;
	month=Apr;
	year=1985;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=0;
	month=0;
	year=0;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=24;
	month=May;
	year=-35;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=23;
	month=-1;
	year=1985;
	date = construct(&year, &month, &day);
	CU_ASSERT_FALSE(isValidDate(date));

	day=23;
	month=Apr;
	year=1985;
	date = construct(&year, &month, &day);
	CU_ASSERT_TRUE(isValidDate(date));

	day=31;
	month=Oct;
	year=1985;
	date = construct(&year, &month, &day);
	CU_ASSERT_TRUE(isValidDate(date));

	day=1;
	month=Jan;
	year=1;
	date = construct(&year, &month, &day);
	CU_ASSERT_TRUE(isValidDate(date));
}

/**
 * @brief Test the calculation of the max amount of days in the month
 *
 * Given Dates that equals (ASSERT_EQUAL):
 * 1.1.1 == 31, 8.2.2016 = 29, 28.2.2017 == 28, 1.3.2017 == 31 ... 1.12.2017 == 31
 */
static void testGetMonthMaxDays(void) {
	day=1;
	month=Jan;
	year=1;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);

	day=8;
	month=Feb;
	year=2016;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 29);

	day=28;
	month=Feb;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 28);

	day=1;
	month=Mar;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);

	day=1;
	month=Apr;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 30);

	day=1;
	month=May;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);

	day=1;
	month=Jun;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 30);

	day=1;
	month=Jul;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);

	day=1;
	month=Aug;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);

	day=1;
	month=Sep;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 30);

	day=1;
	month=Oct;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);

	day=1;
	month=Nov;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 30);

	day=1;
	month=Dec;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(getMonthMaxDays(date), 31);
}

/**
 * @brief Test the calculation the past date from beginning of year
 *
 * Given Dates that equals (ASSERT_EQUAL):
 * 31.12.2016 == 366, 31.12.2017 == 365
 */
void testCountDaysFromBeginningOfYear(void) {
	day=31;
	month=Dec;
	year=2016;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(countDaysFromBeginningOfYear(date), 366);

	day=31;
	month=Dec;
	year=2017;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(countDaysFromBeginningOfYear(date), 365);
}

/**
 * @brief Test the conversion from a date to day (beginnging form 0.0.0)
 *
 * Given Dates that equals (ASSERT_EQUAL):
 * 1.1.1 == 1, 31.12.2018 == 737059
 */
void testConvertToDays(void) {
	day=1;
	month=Jan;
	year=1;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(convertToDays(date), 1);

	day=31;
	month=Dec;
	year=2018;
	date = construct(&year, &month, &day);
	CU_ASSERT_EQUAL(convertToDays(date), 737059);
}

/**
 * @brief Test the conversion from days to a date (beginnging form 0.0.0)
 *
 * Given Dates that equals (ASSERT_EQUAL):
 * 1 == 1.1.1, 737059 == 31.12.2018
 */
void testConvertToDate(void) {
	date = convertToDate(1);
	CU_ASSERT_EQUAL(date.day, 1);
	CU_ASSERT_EQUAL(date.month, Jan);
	CU_ASSERT_EQUAL(date.year, 1);

	date = convertToDate(737059);
	CU_ASSERT_EQUAL(date.day, 31);
	CU_ASSERT_EQUAL(date.month, Dec);
	CU_ASSERT_EQUAL(date.year, 2018);
}

/**
 * @brief Test the addition of days to a date
 */
void testAddDay(void) {
	char* dateAsString = malloc(sizeof("0000-00-00")); // nedded to calculate the size

	int day=2, month=3, year=2014;
	Date date = construct(&year, &month, &day);

	Date newDate = addDay(date, 5);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2014-03-07"));

	date.year = 6; date.month = 2; date.day = 23;
	newDate = addDay(date, -5);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "0006-02-18"));

	date.year = 1984; date.month = 2; date.day = 29;
	newDate = addDay(date, 5);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "1984-03-05"));

	date.year = 2022; date.month = 12; date.day = 30;
	newDate = addDay(date, -4);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	// the given date is not valid so the result is the given date
	CU_ASSERT_TRUE(strcmp(dateAsString, "2022-01-44"));

	date.year = 2022; date.month = 1; date.day = 44;
	newDate = addDay(date, -4);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	// the given date is not valid so the result is the given date
	CU_ASSERT_TRUE(strcmp(dateAsString, "2022-01-44"));

	date.year = 2022; date.month = 1; date.day = 21;
	newDate = addDay(date, -198);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2021-07-7"));

	date.year = 2022; date.month = 2; date.day = 29;
	newDate = addDay(date, -500);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2020-10-16"));

	date.year = 2020; date.month = 2; date.day = 29;
	newDate = addDay(date, -500);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2018-10-16"));

	date.year = 2022; date.month = 12; date.day = 31;
	newDate = addDay(date, -500);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2021-08-18"));

	date.year = 2021; date.month = 12; date.day = 31;
	newDate = addDay(date, -135);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2021-08-18"));

	date.year = 2018; date.month = 12; date.day = 29;
	newDate = addDay(date, 4);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2018-03-04"));

	date.year = 2018; date.month = 1; date.day = 13;
	newDate = addDay(date, -20);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2017-12-23"));

	date.year = 2018; date.month = 1; date.day = 13;
	newDate = addDay(date, 0);
	sprintf(dateAsString, "%04d-%02d-%02d\n", newDate.year, newDate.month, newDate.day);
	CU_ASSERT_TRUE(strcmp(dateAsString, "2018-01-13"));
}


/**
 * @brief Registers and runs the tests.
 *
 * @return EXIT_SUCCES
 */
int main(void)
{
	TestMainBasic("ProgC P2 - Add Day Tests", setup, teardown
				  , testIsLeapYear
				  , testIsValidDate
				  , testGetMonthMaxDays
				  , testCountDaysFromBeginningOfYear
				  , testConvertToDays
				  , testConvertToDate
				  , testAddDay
				  );

	return 0;
}
