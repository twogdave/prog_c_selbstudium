#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @file
 * @brief Calculates Fahrenheit to Celsius
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P1 - Aufgabe 1
 */

/**
 * @brief Aufgabe 1: Converting Fahrenheit to Celsius
 *
 * Description: This functions calculates and prints
 *              Fahrenheit temperature beginning from -100 to 200 in Celsius
 */
void printFahrenheitToCelsius()
{
	printf("F'heit%10sCelsius\n", " ");
	printf("-----------------------\n");

	int fahrenheit;
	double celsius;
	for (fahrenheit = -100; fahrenheit <= 200; fahrenheit += 20)
	{
		celsius = ((double) 5 / 9) * (fahrenheit - 32);
		printf("%6d%10s%7.2f\n", fahrenheit, " ", celsius);
	}
}

/**
 * @brief Aufgabe 1: Main-Method to start Converting Fahrenheit to Celsius
 *
 * @return EXIT_SUCCES
 */
int main(void) {

	printFahrenheitToCelsius();

	return EXIT_SUCCESS;
}


