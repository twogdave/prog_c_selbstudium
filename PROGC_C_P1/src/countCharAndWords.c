#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/**
 * @file
 * @brief Counts and prints all the entered chars and words entered into the console
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P1 - Aufgabe 2
 */

/**
 * @brief Count Chars and Words (input from the console)
 *
 * Description: This functions counts all the console input characters and words
 * 				and prints the result to the console
 */
void getAndPrintCountCharsAndWords()
{
	int totalChars = 0;
	int totalCharsWithoutSpaces = 0;
	int totalWords = 0;

	int previous=32;
	int current;

	while ((current = getchar()) != '\n')
	{
		totalChars++;

		if(!isspace(current))
		{
			totalCharsWithoutSpaces++;
		}

		// if current is not a space and the previous is a space than it is a word
		if (!isspace(current) && isspace(previous))
		{
			totalWords++;
		}

		previous = current;
	}

	printf("\nWords | Chars | Chars (without Spaces)\n");
	printf("%5d | %5d | %22d\n", totalWords, totalChars, totalCharsWithoutSpaces);
}

/**
 * @brief Main-Method to start Count Char and Words
 *
 * @return EXIT_SUCCES
 */
int main(void)
{
	printf("Please enter some words!\n");
	fflush(stdout);
	getAndPrintCountCharsAndWords();
	printf("\nBye Bye!\n");

	return EXIT_SUCCESS;
}
