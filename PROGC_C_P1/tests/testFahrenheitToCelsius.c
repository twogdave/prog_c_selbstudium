#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"
#include "test_utils.h"

/**
 * @file
 * @brief Testing routines to the the file countCharAndWords.c
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P1 - Aufgabe 1
 */

/** @brief Runs only if TARGET1 is defined in make file */
#ifndef TARGET1 // must be given by the make file --> see test target
	#error TARGET1 is not definded
#endif

/** @brief The path of the STDOUT text file. */
#define OUTFILE "../resources/test/output/ftc-stdout.txt"

/** @brief The path of the STDERR text file. */
#define ERRFILE "../resources/test/output/ftc-stderr.txt"

/**
 * @brief setup for the test
 *
 * Delete the outputfiles
 */
static int setup(void)
{
	remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
	return 0;
}

/**
 * @brief teardown for the test
 *
 * Nothing to do
 */
static int teardown(void)
{
	// nothing todo.
	return 0; // success
}


static void testPrintFahrenheitToCelsius(void)
{
	// arrange
	const char *ouputText[] = {
			 "F'heit          Celsius\n"
			,"-----------------------\n"
			,"  -100           -73.33\n"
			,"   -80           -62.22\n"
			,"   -60           -51.11\n"
			,"   -40           -40.00\n"
			,"   -20           -28.89\n"
			,"     0           -17.78\n"
			,"    20            -6.67\n"
			,"    40             4.44\n"
			,"    60            15.56\n"
			,"    80            26.67\n"
			,"   100            37.78\n"
			,"   120            48.89\n"
			,"   140            60.00\n"
			,"   160            71.11\n"
			,"   180            82.22\n"
			,"   200            93.33\n"
	};

	const char *errorText[] = { };
	// act
	int exitCode = system(XSTR(TARGET1) " 1>" OUTFILE " 2>" ERRFILE);
	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

/**
 * @brief Registers and runs the tests.
 *
 * @return EXIT_SUCCES
 */
int main(void)
{
	// setup, run, teardown
	TestMainBasic("ProgC P1 - Aufgabe 1", setup, teardown
				  , testPrintFahrenheitToCelsius
				  );

	return 0;
}
