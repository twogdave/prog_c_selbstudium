#include <stdio.h>
#include <stdlib.h>
#include "CUnit/Basic.h"
#include "test_utils.h"

/**
 * @file
 * @brief Testing routines to the the file countCharAndWords.c
 *
 * @author giuntdav (Davide Giunta)
 * @date 2018-10-11 (HS2018)
 *
 * ZHAW - PROGC Praktikum - PROGC_C_P1 - Aufgabe 2
 */

/** @brief Runs only if TARGET2 is defined in make file */
#ifndef TARGET2 // must be given by the make file --> see test target
	#error TARGET2 is not definded
#endif

/** @brief The path of the STDOUT text file. */
#define OUTFILE "../resources/test/output/ccaw-stdout.txt"

/** @brief The path of the STDERR text file. */
#define ERRFILE "../resources/test/output/ccaw-stderr.txt"


/** @brief The path of the basic input for the test */
#define INFILE_BASIC "../resources/test/input/ccaw-basic.input"
/** @brief The path of the leading blanks input for the test */
#define INFILE_LEADING_BLANKS "../resources/test/input/ccaw-leading-blanks.input"
/** @brief The path of the mixed spaces input for the test */
#define INFILE_MIXED_SPACES "../resources/test/input/ccaw-mixed-spaces.input"
/** @brief The path of the basic mutliple words for the test */
#define INFILE_MULTIPLE_WORDS "../resources/test/input/ccaw-multiple-words.input"
/** @brief The path of the basic tabs for the test */
#define INFILE_TABS "../resources/test/input/ccaw-tabs.input"
/** @brief The path of the basic trailing blanks for the test */
#define INFILE_TRAILING_BLANKS "../resources/test/input/ccaw-trailing-blanks.input"


/**
 * @brief setup for the test
 *
 * Delete the outputfiles
 */
static int setup(void)
{
	remove_file_if_exists(OUTFILE);
	remove_file_if_exists(ERRFILE);
 	return 0;
}
/**
 * @brief teardown for the test
 *
 * Nothing to do
 */
static int teardown(void)
{
	// nothing todo.
	return 0;
}

/**
 * @brief Test with basic input
 *
 * Only one word
 */
static void testCountCharsAndWordsInputBasic(void)
{
	// arrange
	const char *ouputText[] = {
			"\nWords | Chars | Chars (without Spaces)\n",
			  "    1 |     5 |                      5\n"
	};
	const char *errorText[] = { };

	// act
	int exitCode = system(XSTR(TARGET2) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_BASIC);

	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

/**
 * @brief Test with leading blanks input
 *
 * one word with leading blanks
 */
static void testCountCharsAndWordsInputLeadingBlanks(void)
{
	// arrange
	const char *ouputText[] = {
			"\nWords | Chars | Chars (without Spaces)\n",
              "    1 |     8 |                      5\n"
	};
	const char *errorText[] = { };

	// act
	int exitCode = system(XSTR(TARGET2) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_LEADING_BLANKS);

	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

/**
 * @brief Test with mixed spaces input
 *
 * multiple words with one or more spaces between
 */
static void testCountCharsAndWordsInputMixedSpaces(void)
{
	// arrange
	const char *ouputText[] = {
			"\nWords | Chars | Chars (without Spaces)\n",
              "    6 |    31 |                     21\n"
	};
	const char *errorText[] = { };

	// act
	int exitCode = system(XSTR(TARGET2) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_MIXED_SPACES);

	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

/**
 * @brief Test with mutliple word input
 *
 * multiple words
 */
static void testCountCharsAndWordsInputMultipleWords(void)
{
	// arrange
	const char *ouputText[] = {
			"\nWords | Chars | Chars (without Spaces)\n",
              "    2 |    10 |                      9\n"
	};
	const char *errorText[] = { };

	// act
	int exitCode = system(XSTR(TARGET2) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_MULTIPLE_WORDS);

	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

/**
 * @brief Test with tabs input
 *
 * multiple words with one or more tabs between
 */
static void testCountCharsAndWordsInpuTabs(void)
{
	// arrange
	const char *ouputText[] = {
			"\nWords | Chars | Chars (without Spaces)\n",
			  "    2 |    12 |                      9\n"
	};
	const char *errorText[] = { };

	// act
	int exitCode = system(XSTR(TARGET2) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_TABS);

	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

static void testCountCharsAndWordsInputTrailingBlanks(void)
{
	// arrange
	const char *ouputText[] = {
			"\nWords | Chars | Chars (without Spaces)\n",
			  "    1 |     8 |                      5\n"
	};
	const char *errorText[] = { };

	// act
	int exitCode = system(XSTR(TARGET2) " 1>" OUTFILE " 2>" ERRFILE " <" INFILE_TRAILING_BLANKS);

	// assert
	CU_ASSERT_EQUAL(exitCode, 0);
	assert_lines(OUTFILE, ouputText, sizeof(ouputText)/sizeof(*ouputText));
	assert_lines(ERRFILE, errorText, sizeof(errorText)/sizeof(*errorText));
}

/**
 * @brief Registers and runs the tests.
 *
 * @return EXIT_SUCCES
 */
int main(void)
{
	TestMainBasic("ProgC P1 - Aufgabe 2", setup, teardown
				  , testCountCharsAndWordsInputBasic
                  , testCountCharsAndWordsInputLeadingBlanks
                  , testCountCharsAndWordsInputMixedSpaces
                  , testCountCharsAndWordsInputMultipleWords
                  , testCountCharsAndWordsInputTrailingBlanks
                  , testCountCharsAndWordsInpuTabs
				  );

	return 0;
}
